from .base import BaseFE
import numpy

class NumericalFE(BaseFE):
    def __init__(self,moyenne=None,ecart_type=None):
        super().__init__()
        #### TODO: initialize the numerical feature transformation OK
        self.liste = []
        self.moyenne = moyenne
        self.ecart_type = ecart_type


    def fit(self, data):
        #### TODO: find parameters for the numerical feature transformation OK
        for v in data:
            if not isinstance(v, float) and not isinstance(v,int):
                break   ## TODO : supprimer la ligne si il y a une erreur
            self.liste.append(v)
        self.moyenne=numpy.mean(self.liste)
        self.ecart_type=numpy.std(self.liste)

    def transform_fn(self, x):
        return (x-self.moyenne)/self.ecart_type
    
    def transform(self, data):
        #### TODO: apply the numerical feature transformation to new data OK
        return data.map(self.transform_fn)