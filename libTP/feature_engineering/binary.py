import numpy as np

from .base import BaseFE
import pandas as pd


class BinaryFE(BaseFE):
    def __init__(self):
        super().__init__()
        #### TODO: initialize the binary feature transformation : OK
        self.rows_to_transform = []

    def fit(self, data):
        #### TODO: find parameters for the binary feature transformation : OK
        cpt = 0
        for v in data:
            # If the binary attribute has a value different from 0 and 1, the line will be deleted
            if v != 0 and v != 1:
                self.rows_to_transform.append(cpt)

            cpt += 1

    def transform(self, data):
        #### TODO: apply the binary feature transformation to new data : OK
        data_transformed = data
        for index in self.rows_to_transform:
            data_transformed[index] = 1

        return data_transformed
