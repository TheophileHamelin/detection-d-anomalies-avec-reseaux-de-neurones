import json
import os
import time

import libTP
from .categorical import CategoricalFE
from .numerical import NumericalFE
from .binary import BinaryFE
import pandas as pd

### takes as input a dictionary in the form {attr: FeatureExtractor} and save to disk
def save_transforms(transforms, base_path="./conf/fe"):
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    # TODO implement transforms saving/loading functions : OK
    for attr, fe in transforms.items():
        fe.save(base_path+"/"+attr)


### returns a dictionary in the form {attr: FeatureExtractor}
def load_transforms(conf, base_path='./conf/fe'):
    # TODO implement transforms saving/loading functions
    transforms = {}
    for attr, t in conf["attributes"].items():
        # On instancie un XyzFE en fonction du type de l'attribut
        if t == "categorical":
            transforms[attr] = libTP.feature_engineering.CategoricalFE()
        if t == "numerical":
            transforms[attr] = libTP.feature_engineering.NumericalFE()
        if t == "binary":
            transforms[attr] = libTP.feature_engineering.BinaryFE()

        transforms[attr].load(base_path+"/"+attr)

    return transforms

### takes a dataframe and a dictionary in the form {attr: FeatureExtractor} as input 
### returns the transformed dataframe
def transform_df(df, transforms):
    res = pd.DataFrame()
    for attr, fe in transforms.items():
        res[attr] = fe(df[attr])
    return res