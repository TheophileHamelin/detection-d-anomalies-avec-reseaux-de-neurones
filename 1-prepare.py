import json

from torch import numel

import libTP
import pandas as pd  # Librairie pandas qui permet de charger et manipuler facilement la donnée
from libTP.feature_engineering.categorical import CategoricalFE as CFE
from libTP.feature_engineering.binary import BinaryFE as BFE

# from libTP.feature_engineering.numerical import NumericalFE as NFE


###### The objective of this step is to create a configuration JSON file that will be used
###### to setup the autoencoder structure, as well as find and save the parameters for 
###### the feature engineering functions
from libTP.feature_engineering.helpers import save_transforms, load_transforms

conf = {
    # This will store the name of the used attributes and their types 
    "attributes": {},
    # Store the parameters that will be used to build the network structure (ex: input size)
    "network": {}
}

df = pd.read_csv('dataset/train.csv', ',')

cat_attributes = ["proto", "service", "state", "attack_cat"]
num_attributes = ["dur", "spkts", "dpkts", "sbytes", "dbytes", "rate", "sttl", "dttl", "sload", "dload", "sloss",
                  "dloss", "sinpkt", "dinpkt"
    , "synack", "ackdat", "smean", "dmean", "trans_depth", "response_body_len", "ct_srv_src", "ct_dst_ltm",
                  "ct_src_dport_ltm"
    , "ct_dst_sport_ltm", "ct_dst_src_ltm", "ct_ftp_cmd", "ct_flw_http_mthd", "ct_src_ltm", "ct_srv_dst"]
bin_attributes = ["is_ftp_login", "is_sm_ips_ports"]

## a list of all categorical attributes
for attr in cat_attributes:
    conf["attributes"][attr] = "categorical"

## a list of all numerical attributes
for attr in num_attributes:
    conf["attributes"][attr] = "numerical"

## a list of all binary attributes
for attr in bin_attributes:
    conf["attributes"][attr] = "binary"

categorical_transformer = CFE()
binary_transformer = BFE()

# First round to detect the number of rows to delete
for bin_attr in bin_attributes:
    binary_transformer.fit(pd.Series(df[bin_attr].values))

# Deletion of the rows
# for bin_attr in bin_attributes:
#     # Ici sont retournées les valeurs binaires transformées
#     print(binary_transformer.transform(pd.Series(df[bin_attr].values)))

binary_transformer.fit(pd.Series(df[bin_attributes[0]].values))
binary_transformer.transform(pd.Series(df[bin_attributes[0]].values))
# cat_test.fit(pd.Series(dataframe["proto"].values))
# print(cat_test.transform(pd.Series(dataframe["proto"].values)))
# print(cat_test.association_dict)
# print(cat_test.max_size)

###### TODO: create the transformation functions and find their parameters
######       Before that, you should complete the code inside libTP/feature_engineering folder
######       An example is provided for the categorical feature encoder
######       transforms is a dictionary of the form {attribute_name: feature_encoder_object} : OK
transforms = {}
for attr, t in conf["attributes"].items():
    # On instancie un XyzFE en fonction du type de l'attribut
    if t == "categorical":
        transforms[attr] = libTP.feature_engineering.CategoricalFE()
    if t == "numerical":
        transforms[attr] = libTP.feature_engineering.NumericalFE()
    if t == "binary":
        transforms[attr] = libTP.feature_engineering.BinaryFE()
    # On trouve les bons parametres gr^ace a fit et aux donnees
    transforms[attr].fit(df[attr])

###### TODO: Set parameters required to build the network's structure
######       Expected format is as follow:
######       conf["network"] = {
######          attribute_name:  {
######            "type": attribute_type,
######            "output_size": ...,
######            "param1": ...
######            etc...
######       } : OK
for attr, t in conf["attributes"].items():
    conf["network"][attr] = {"type": t, "output_size": 2}
    if t == "categorical":
        conf["network"][attr]["voc_size"] = transforms[attr].max_size
        # Les variables categorielles peuvent profiter d'avoir une output_size plus elevee
        conf["network"][attr]["output_size"] = conf["network"][attr]["voc_size"]

###### TODO: Don't forget to save:
######       - The conf inside conf/config.json
######         You can use python's built-in json module to do so
######       - The fitted feature encoder objects
######         The function is already implemented inside libTP.feature_engineering.helpers module
save_transforms(transforms)
with open('./conf/conf.json', 'w') as json_file:
    # Conf saving
    json.dump(conf, json_file, indent=4)

