# Detection d anomalies avec reseaux de neurones

L’objectif est d’implémenter un réseau de neurone auto-encodeur et de trouver une configuration optimisée afin de répondre à un problème de détection d’anomalie (Apprentissage non-supervisé).

Le dataset utilisé est orienté cybersécurité. Il regroupe plusieurs attributs relatifs à des connexions au sein d’un
système d’information. Certaines de ces connexions sont attribuables à des attaques,
d’autres à du trafic normal. 
